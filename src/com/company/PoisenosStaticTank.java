package com.company;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * kind of enemy that if you go near him you will die
 */
public class PoisenosStaticTank extends StaticTank implements Serializable {
    private int poisonRange;
    private transient BufferedImage redZone;
    public PoisenosStaticTank(int locX, int locY, int tirRange) {
        super(locX, locY, tirRange);
        poisonRange = 200;
        try {
            tank = ImageIO.read(new File("src\\Images\\redZone.png"));

            redZone = tank;

        } catch (IOException e) {
            e.printStackTrace();
        }
        kind = "PoisenosStaticTank";
    }

    public BufferedImage getRedZone() {
        return redZone;
    }

    @Override
    public void update(TankHuman tankHuman) {
        if(count%20 == 0)
        if(Math.pow(locX-tankHuman.getLocX(),2)+Math.pow(locY-tankHuman.getLocY(),2) <= poisonRange*poisonRange){
            GameState.setChanged(true);
            tankHuman.setHealth(tankHuman.getHealth()-20);
        }
    }
}
