package com.company;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

/**
 * this class handle and play and pause all of the music of the game
 */
public class SoundsHandeler {
    public static Clip bacgroundSound;
    public static Clip quickSound;

    /**
     * quick sounds in the game play
     * @param file
     */
    public static void  playSoundInGame(File file) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
            quickSound = AudioSystem.getClip();
            quickSound.open(audioInputStream);
            quickSound.start();
            System.out.println();
//            audioInputStream = AudioSystem.getAudioInputStream(new File("MineBoom.wav").getAbsoluteFile());
//                    clip.start();
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    /**
     * background sound will played
     * @param file
     */
    public static void  playSoundBackGround(File file){
        try {
            try {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
                bacgroundSound = AudioSystem.getClip();
                bacgroundSound.open(audioInputStream);
                bacgroundSound.loop(10);
                bacgroundSound.start();
            } catch (Exception ex) {
                System.out.println("Error with playing sound.");
                ex.printStackTrace();
            }
//                    clip.start();
        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    public static void quickSoundStop(){
        quickSound.stop();
    }
    public static void backGroundSoundStop(){
        bacgroundSound.stop();
    }
    public static void backGroundSoundResume(){
        bacgroundSound.start();
    }
}
