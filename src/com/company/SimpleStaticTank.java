package com.company;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * kind of enemies can not move
 */
public class SimpleStaticTank extends StaticTank {

    public SimpleStaticTank(int locX, int locY, int tirRange) {
        super(locX, locY, tirRange);
        try {
            tank = ImageIO.read(new File("src\\Images\\wicket2.png"));
            looleh = ImageIO.read(new File("Src//looleh.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        kind = "SimpleStaticTank";
    }
}
